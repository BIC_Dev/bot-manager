package configs

import (
	"context"
	"os"

	"gitlab.com/BIC_Dev/nitrado-server-manager-v3/utils/logging"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	// Redis struct {
	// 	Host string `yaml:"host"`
	// 	Port int    `yaml:"port"`
	// 	Pool int    `yaml:"pool"`
	// } `yaml:"REDIS"`
	GuildConfigService struct {
		Host     string `yaml:"host"`
		BasePath string `yaml:"base_path"`
	} `yaml:"GUILD_CONFIG_SERVICE"`
	NitradoServerManager struct {
		Host     string `yaml:"host"`
		BasePath string `yaml:"base_path"`
	} `yaml:"NITRADO_SERVER_MANAGER"`
	Bot struct {
		Prefix           string `yaml:"prefix"`
		OkColor          int    `yaml:"ok_color"`
		WarnColor        int    `yaml:"warn_color"`
		ErrorColor       int    `yaml:"error_color"`
		DocumentationURL string `yaml:"documentation_url"`
		GuildService     string `yaml:"guild_service"`
		WorkingThumbnail string `yaml:"working_thumbnail"`
		OkThumbnail      string `yaml:"ok_thumbnail"`
		WarnThumbnail    string `yaml:"warn_thumbnail"`
		ErrorThumbnail   string `yaml:"error_thumbnail"`
	} `yaml:"BOT"`
	CacheSettings struct {
		SlashCommand CacheSetting `yaml:"slash_command"`
	}
	Commands []Command `yaml:"COMMANDS"`
}

// CacheSetting struct
type CacheSetting struct {
	Base    string `yaml:"base"`
	TTL     string `yaml:"ttl"`
	Enabled bool   `yaml:"enabled"`
}

// Command struct
type Command struct {
	Name        string          `yaml:"name"`
	SlashName   string          `yaml:"slash_name"`
	Description string          `yaml:"description"`
	Options     []CommandOption `yaml:"options"`
	Examples    []string        `yaml:"examples"`
	Enabled     bool            `yaml:"enabled"`
	Workers     int             `yaml:"workers"`
	RequireAuth bool            `yaml:"require_auth"`
}

// CommandOption struct
type CommandOption struct {
	Type        string                 `yaml:"type"`
	Name        string                 `yaml:"name"`
	Description string                 `yaml:"description"`
	Required    bool                   `yaml:"required"`
	Choices     []CommandOptionChoices `yaml:"choices"`
}

// CommandOptionChoices struct
type CommandOptionChoices struct {
	Name  string      `yaml:"name"`
	Value interface{} `yaml:"value"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(ctx context.Context, env string) *Config {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &config
}
