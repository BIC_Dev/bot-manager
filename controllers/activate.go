package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/bot-manager/viewmodels"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_services"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guilds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"go.uber.org/zap"
)

func (c *Controller) ActivateBot(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.ActivateBotRequest
	dcErr := json.NewDecoder(r.Body).Decode(&body)
	if dcErr != nil {
		Error(ctx, w, "Failed to decode request body", dcErr, http.StatusBadRequest)
		return
	}

	if !c.DiscordSession.StateEnabled {
		Error(ctx, w, "Discord state is not enabled", errors.New("discord state not enabled"), http.StatusNotFound)
		return
	}

	discordGuilds := c.DiscordSession.State.Guilds
	if discordGuilds == nil {
		Error(ctx, w, "No guilds in Discord state", errors.New("discord state has no guilds"), http.StatusNotFound)
		return
	}

	var guild *discordgo.Guild
	for _, aGuild := range discordGuilds {
		if aGuild.ID == body.GuildID {
			guild = aGuild
			break
		}
	}

	if guild == nil {
		Error(ctx, w, "Bot is not in specified guild", errors.New("guild not found"), http.StatusNotFound)
		return
	}

	guildNeedsCreation := false
	guildNeedsGuildService := false
	guildNeedsGuildServiceActivation := false
	var guildService *gcscmodels.GuildService

	guildFeedParams := guild_feeds.NewGetGuildFeedByIDParamsWithTimeout(30)
	guildFeedParams.Guild = guild.ID
	guildFeedParams.GuildID = guild.ID
	guildFeedParams.Context = context.Background()

	guildFeed, gfErr := c.GuildConfigService.Client.GuildFeeds.GetGuildFeedByID(guildFeedParams, c.GuildConfigService.Auth)
	if gfErr != nil {
		if _, ok := gfErr.(*guild_feeds.GetGuildFeedByIDNotFound); !ok {
			Error(ctx, w, "Failed to find bot information", gfErr, http.StatusNotFound)
			return
		}

		guildNeedsCreation = true
		guildNeedsGuildService = true
	} else {
		if guildFeed.Payload == nil {
			Error(ctx, w, "Failed to find bot information", errors.New("guild feed nil payload"), http.StatusNotFound)
			return
		}

		if guildFeed.Payload.Guild == nil {
			Error(ctx, w, "Failed to find bot information", errors.New("guild feed nil guild"), http.StatusNotFound)
			return
		}

		if guildFeed.Payload.Guild.Enabled == false {
			Error(ctx, w, "Discord server not enabled to use BIC Development bots", errors.New("guild disabled"), http.StatusNotFound)
			return
		}

		if guildFeed.Payload.Guild.GuildServices == nil {
			guildNeedsGuildService = true
		} else {
			guildServiceExists := false
			for _, aGuildService := range guildFeed.Payload.Guild.GuildServices {
				if aGuildService.Name == c.Config.Bot.GuildService {
					guildService = aGuildService
					guildServiceExists = true
					if aGuildService.Enabled == false {
						guildNeedsGuildServiceActivation = true
					}
					break
				}
			}

			if !guildServiceExists {
				guildNeedsGuildService = true
			}
		}
	}

	dGuild, dgErr := c.DiscordSession.Guild(guild.ID)
	if dgErr != nil {
		Error(ctx, w, "Unable to get Discord server information", dgErr, http.StatusNotFound)
		return
	}

	if guildNeedsCreation {
		guildBody := gcscmodels.Guild{
			ID:      guild.ID,
			Name:    dGuild.Name,
			Enabled: true,
		}
		createGuildParams := guilds.NewCreateGuildParamsWithTimeout(10)
		createGuildParams.SetContext(context.Background())
		createGuildParams.SetBody(&guildBody)
		_, cgErr := c.GuildConfigService.Client.Guilds.CreateGuild(createGuildParams, c.GuildConfigService.Auth)
		if cgErr != nil {
			Error(ctx, w, "Failed to add Discord server to bot", cgErr, http.StatusNotFound)
			return
		}
	}

	if guildNeedsGuildService {
		guildServiceBody := gcscmodels.GuildService{
			GuildID: guild.ID,
			Name:    c.Config.Bot.GuildService,
			Enabled: true,
		}
		createGuildServiceParams := guild_services.NewCreateGuildServiceParamsWithTimeout(10)
		createGuildServiceParams.SetContext(context.Background())
		createGuildServiceParams.SetGuild(guild.ID)
		createGuildServiceParams.SetBody(&guildServiceBody)
		_, cgsErr := c.GuildConfigService.Client.GuildServices.CreateGuildService(createGuildServiceParams, c.GuildConfigService.Auth)
		if cgsErr != nil {
			Error(ctx, w, "Failed to enable "+c.Config.Bot.GuildService, cgsErr, http.StatusNotFound)
			return
		}
	} else if guildNeedsGuildServiceActivation {
		guildServiceBody := gcscmodels.UpdateGuildServiceRequest{
			Enabled: true,
			GuildID: guildService.GuildID,
			Name:    guildService.Name,
		}
		updateGuildServiceParams := guild_services.NewUpdateGuildServiceParamsWithTimeout(10)
		updateGuildServiceParams.SetGuild(guild.ID)
		updateGuildServiceParams.SetGuildServiceID(int64(guildService.ID))
		updateGuildServiceParams.SetContext(context.Background())
		updateGuildServiceParams.SetBody(&guildServiceBody)

		_, ugsErr := c.GuildConfigService.Client.GuildServices.UpdateGuildService(updateGuildServiceParams, c.GuildConfigService.Auth)
		if ugsErr != nil {
			Error(ctx, w, "Failed to enable "+c.Config.Bot.GuildService, ugsErr, http.StatusNotFound)
			return
		}
	}

	Response(ctx, w, viewmodels.ActivateBotResponse{
		Message: "Activated",
	}, http.StatusOK)
}
