package handlers

import (
	"context"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/handlers/commands"
	"gitlab.com/BIC_Dev/bot-manager/handlers/components"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/services/nsm"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"go.uber.org/zap"
)

// Handlers struct
type Handlers struct {
	Session                  *discordgo.Session
	Config                   *configs.Config
	GuildConfigService       *guildconfigservice.GuildConfigService
	NitradoServerManager     *nsm.NSM
	MessagesAwaitingReaction *components.MessagesAwaitingReaction
	Commands                 *commands.Commands
	Components               *components.Components
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
	Code    int    `json:"code"`
}

// Error func
func (ie *Error) Error() string {
	return ie.Err.Error()
}

func (h *Handlers) AddSlashCommands(ctx context.Context, env string) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var appCommands []*discordgo.ApplicationCommand

	for _, command := range h.Config.Commands {
		if !command.Enabled {
			continue
		}

		appCommand := &discordgo.ApplicationCommand{
			ApplicationID: h.Session.State.User.ID,
			Name:          command.SlashName,
			Description:   command.Description,
		}

		var options []*discordgo.ApplicationCommandOption
		for _, option := range command.Options {
			var aType discordgo.ApplicationCommandOptionType
			switch option.Type {
			case "STRING":
				aType = discordgo.ApplicationCommandOptionString
			case "INTEGER":
				aType = discordgo.ApplicationCommandOptionInteger
			case "BOOLEAN":
				aType = discordgo.ApplicationCommandOptionBoolean
			case "USER":
				aType = discordgo.ApplicationCommandOptionUser
			case "CHANNEL":
				aType = discordgo.ApplicationCommandOptionChannel
			case "ROLE":
				aType = discordgo.ApplicationCommandOptionRole
			default:
				continue
			}

			var choices []*discordgo.ApplicationCommandOptionChoice
			for _, choice := range option.Choices {
				choices = append(choices, &discordgo.ApplicationCommandOptionChoice{
					Name:  choice.Name,
					Value: choice.Value,
				})
			}

			options = append(options, &discordgo.ApplicationCommandOption{
				Type:        aType,
				Name:        option.Name,
				Description: option.Description,
				Required:    option.Required,
				Choices:     choices,
			})

			appCommand.Options = options
		}

		appCommands = append(appCommands, appCommand)
	}

	if env == "local" {
		for _, guild := range h.Session.State.Guilds {
			newCommands, ncmdErr := h.Session.ApplicationCommandBulkOverwrite(h.Session.State.User.ID, guild.ID, appCommands)
			if ncmdErr != nil {
				nCtx := logging.AddValues(ctx, zap.NamedError("error", ncmdErr), zap.String("error_message", "failed to add new guild slash commands"))
				logger := logging.Logger(nCtx)
				logger.Error("error_log")
			} else {
				nCtx := logging.AddValues(ctx, zap.String("message", fmt.Sprintf("Added %d guild slash commands", len(newCommands))))
				logger := logging.Logger(nCtx)
				logger.Info("setup_log")
			}
		}
	} else if env == "prod" {
		newCommands, ncmdErr := h.Session.ApplicationCommandBulkOverwrite(h.Session.State.User.ID, "", appCommands)
		if ncmdErr != nil {
			nCtx := logging.AddValues(ctx, zap.NamedError("error", ncmdErr), zap.String("error_message", "failed to add new global slash commands"))
			logger := logging.Logger(nCtx)
			logger.Error("error_log")
		} else {
			nCtx := logging.AddValues(ctx, zap.String("message", fmt.Sprintf("Added %d global slash commands", len(newCommands))))
			logger := logging.Logger(nCtx)
			logger.Info("setup_log")
		}
	}
}

// SetupHandlers func
func (h *Handlers) SetupHandlers() {
	h.MessagesAwaitingReaction = &components.MessagesAwaitingReaction{
		Messages: make(map[string]components.MessageAwaitingReaction),
	}

	go components.ExpireMessagesAwaitingReaction(h.MessagesAwaitingReaction)

	commands := &commands.Commands{
		Session:                  h.Session,
		Config:                   h.Config,
		GuildConfigService:       h.GuildConfigService,
		NitradoServerManager:     h.NitradoServerManager,
		MessagesAwaitingReaction: h.MessagesAwaitingReaction,
	}

	components := &components.Components{
		Session:                  h.Session,
		Config:                   h.Config,
		GuildConfigService:       h.GuildConfigService,
		NitradoServerManager:     h.NitradoServerManager,
		MessagesAwaitingReaction: h.MessagesAwaitingReaction,
	}

	h.Commands = commands
	h.Components = components

	h.Session.AddHandler(h.InteractionHandler)
}

func (h *Handlers) InteractionHandler(s *discordgo.Session, ic *discordgo.InteractionCreate) {
	ctx := context.Background()
	if ic.Type == 2 {
		h.Commands.Factory(ctx, s, ic)
	}
	// if ic.Type == 3 {
	// 	switch ic.MessageComponentData().CustomID {
	// 	}
	// }
}
