package commands

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"go.uber.org/zap"
)

// ActivationTokenSuccessOutput
type ActivationTokenSuccessOutput struct {
	BotType         string
	ActivationToken string
}

func (c *Commands) ActivationToken(ctx context.Context, s *discordgo.Session, guildFeed *guild_feeds.GetGuildFeedByIDOK, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if _, ok := options["bot"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO BOT",
			Err:     errors.New("no bot provided"),
		})
		return
	}

	var activationTokenSuccessOutput *ActivationTokenSuccessOutput

	switch options["bot"] {
	case "Nitrado Server Manager":
		nsmActivation, err := c.NitradoServerManager.CreateActivationToken()
		if err != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: err.Message,
				Err:     err.Err,
			})
			return
		}

		activationTokenSuccessOutput = &ActivationTokenSuccessOutput{
			BotType:         "Nitrado Server Manager",
			ActivationToken: nsmActivation.SetupToken.Token,
		}
	default:
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "UNKNOWN BOT TYPE",
			Err:     errors.New("invalid bot type provided"),
		})
		return
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	if activationTokenSuccessOutput != nil {
		embeddableFields = append(embeddableFields, activationTokenSuccessOutput)
	}

	embedParams := discordapi.EmbeddableParams{
		Title:       "Created Activation Token",
		Description: fmt.Sprintf("Please use the activation token below to activate the %s bot on your Discord.", activationTokenSuccessOutput.BotType),
		TitleURL:    c.Config.Bot.DocumentationURL,
		Footer:      fmt.Sprintf("Executed by %s", ic.Member.User.Username),
	}

	c.SuccessOutput(ctx, ic, embedParams, embeddableFields, embeddableErrors)
}

// ConvertToEmbedField for ActivationTokenSuccessOutput struct
func (so *ActivationTokenSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	return &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("__**%s**__", so.BotType),
		Value:  fmt.Sprintf("**Activation Token:** `%s`", so.ActivationToken),
		Inline: false,
	}, nil
}
