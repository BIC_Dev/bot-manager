package commands

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"go.uber.org/zap"
)

// AddRoleSuccessOutput
type AddRoleSuccessOutput struct {
	Role    string
	Command string
}

// AddRoleErrorOutput
type AddRoleErrorOutput struct {
	Role    string
	Command string
}

func (c *Commands) AddRole(ctx context.Context, s *discordgo.Session, guildFeed *guild_feeds.GetGuildFeedByIDOK, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if _, ok := options["command"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO COMMAND",
			Err:     errors.New("no command provided"),
		})
		return
	}

	if _, ok := options["role"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO ROLE",
			Err:     errors.New("no role provided"),
		})
		return
	}

	commandName := options["command"].(string)
	roleID := options["role"].(string)

	var foundCommand *configs.Command
	for _, aCommand := range c.Config.Commands {
		if !aCommand.Enabled {
			continue
		}

		if aCommand.Name == commandName {
			foundCommand = &aCommand
			break
		}
	}

	if foundCommand == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "INVALID COMMAND",
			Err:     errors.New("command does not exist"),
		})
		return
	}

	var guildService *gcscmodels.GuildService
	for _, aGuildService := range guildFeed.Payload.Guild.GuildServices {
		if aGuildService.Name == c.Config.Bot.GuildService {
			guildService = aGuildService
			break
		}
	}

	if guildService == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILD SERVICE",
			Err:     errors.New("missing guild service"),
		})
		return
	}

	var accessAlreadyGranted bool = false
	if guildService.GuildServicePermissions == nil {
		accessAlreadyGranted = false
	} else {
		for _, permission := range guildService.GuildServicePermissions {
			if permission.RoleID == roleID && permission.CommandName == foundCommand.Name {
				accessAlreadyGranted = true
				break
			}
		}
	}

	if accessAlreadyGranted {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "ROLE ALREADY HAS ACCESS",
			Err:     errors.New("role already has access"),
		})
		return
	}

	var successOutput *AddRoleSuccessOutput
	var errorOutput *AddRoleErrorOutput

	_, gspErr := guildconfigservice.CreateGuildServicePermission(ctx, c.GuildConfigService, ic.GuildID, guildService.ID, roleID, foundCommand.Name)
	if gspErr != nil {
		newCtx := logging.AddValues(ctx,
			zap.NamedError("error", gspErr),
			zap.String("error_message", gspErr.Message),
			zap.String("role_id", roleID),
			zap.String("permission_command", commandName),
		)
		logger := logging.Logger(newCtx)
		logger.Error("error_log")

		errorOutput = &AddRoleErrorOutput{
			Command: commandName,
			Role:    roleID,
		}
	} else {
		successOutput = &AddRoleSuccessOutput{
			Command: commandName,
			Role:    roleID,
		}
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	if successOutput != nil {
		embeddableFields = append(embeddableFields, successOutput)
	}

	if errorOutput != nil {
		embeddableErrors = append(embeddableErrors, errorOutput)
	}

	embedParams := discordapi.EmbeddableParams{
		Title:       "Added Role Access",
		Description: fmt.Sprintf("Added role access for: <@&%s>.", roleID),
		TitleURL:    c.Config.Bot.DocumentationURL,
		Footer:      fmt.Sprintf("Executed by %s", ic.Member.User.Username),
	}

	c.SuccessOutput(ctx, ic, embedParams, embeddableFields, embeddableErrors)

	return
}

// ConvertToEmbedField for AddRoleSuccessOutput struct
func (so *AddRoleSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if so.Command != "" {
		fieldVal = "```\n" + so.Command + "\n```"
	} else {
		fieldVal = "```\nUnknown\n```"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Provided Access",
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for AddRoleErrorOutput struct
func (eo *AddRoleErrorOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if eo.Command != "" {
		fieldVal = "```\n" + eo.Command + "\n```"
	} else {
		fieldVal = "```\nUnknown\n```"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Failed Access",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
