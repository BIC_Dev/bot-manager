package commands

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/handlers/components"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/services/nsm"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"go.uber.org/zap"
)

// Commands struct
type Commands struct {
	Session                  *discordgo.Session
	Config                   *configs.Config
	GuildConfigService       *guildconfigservice.GuildConfigService
	NitradoServerManager     *nsm.NSM
	MessagesAwaitingReaction *components.MessagesAwaitingReaction
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
}

// Error func
func (e *Error) Error() string {
	return e.Err.Error()
}

// Factory func
func (c *Commands) Factory(ctx context.Context, s *discordgo.Session, ic *discordgo.InteractionCreate) {
	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
	)

	options := parseSlashCommand(ic)

	ctx = logging.AddValues(ctx,
		zap.Any("options", options),
		zap.String("command", ic.ApplicationCommandData().Name),
	)

	command, gcErr := getCommand(c.Config.Commands, ic.ApplicationCommandData().Name)
	if gcErr != nil {
		if gcErr.Error() == "not a command" {
			return
		}

		ctx = logging.AddValues(ctx, zap.NamedError("error", gcErr.Err), zap.String("error_message", gcErr.Message))
		logger := logging.Logger(ctx)
		logger.Error("error_log")
		return
	}

	if !command.Enabled {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "This command has not been enabled for use",
			Err:     errors.New("command not enabled"),
		})
		return
	}

	ctx = logging.AddValues(ctx,
		zap.String("command", command.Name),
	)

	logger := logging.Logger(ctx)
	logger.Info("command_log")

	var guildFeed *guild_feeds.GetGuildFeedByIDOK
	var gfErr *guildconfigservice.Error

	// Used for when we don't want to get the guild feed this way
	switch command.Name {
	default:
		guildFeed, gfErr = guildconfigservice.GetGuildFeed(ctx, c.GuildConfigService, ic.GuildID)
		if gfErr != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: gfErr.Message,
				Err:     gfErr,
			})
			return
		}

		if vErr := guildconfigservice.ValidateGuildFeed(guildFeed, c.Config.Bot.GuildService, "GuildServices"); vErr != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: vErr.Message,
				Err:     vErr,
			})
			return
		}
	}

	if command.RequireAuth {
		approved, appErr := c.IsApproved(ctx, ic, guildFeed, command)
		if appErr != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: appErr.Message,
				Err:     appErr,
			})
			return
		}

		if !approved {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: "NO ACCESS",
				Err:     errors.New("missing access to command"),
			})
			return
		}
	}

	switch command.Name {
	// case "Help":
	// 	c.Help(ctx, s, ic, options, command)
	case "Add Role":
		c.AddRole(ctx, s, guildFeed, ic, options, command)
	case "Remove Role":
		c.RemoveRole(ctx, s, guildFeed, ic, options, command)
	case "Activation Token":
		c.ActivationToken(ctx, s, guildFeed, ic, options, command)
	case "Guild Info":
		c.GuildInfo(ctx, s, guildFeed, ic, options, command)
	case "User Guild":
		c.UserGuild(ctx, s, guildFeed, ic, options, command)
	default:
		// TODO: Output error
	}
}

// getCommand func
func getCommand(commands []configs.Command, name string) (configs.Command, *Error) {
	for _, val := range commands {
		if val.SlashName == strings.ToLower(name) {
			return val, nil
		}
	}

	return configs.Command{}, &Error{
		Message: "No command found",
		Err:     fmt.Errorf("%s command not found", name),
	}
}

func (c *Commands) ErrorOutput(ctx context.Context, command configs.Command, ic *discordgo.InteractionCreate, err Error) *Error {
	ctx = logging.AddValues(ctx,
		zap.NamedError("error", err.Err),
		zap.String("error_message", err.Message),
	)

	logger := logging.Logger(ctx)
	logger.Error("error_log")

	params := discordapi.EmbeddableParams{
		Title:        "Error",
		Description:  "`" + command.Name + "`",
		Color:        c.Config.Bot.ErrorColor,
		TitleURL:     c.Config.Bot.DocumentationURL,
		Footer:       ic.Member.User.Username,
		ThumbnailURL: c.Config.Bot.ErrorThumbnail,
	}

	var embeddableFields []discordapi.EmbeddableField

	embeddableFields = append(embeddableFields, &err)
	// embeddableFields = append(embeddableFields, &HelpOutput{
	// 	Command: command,
	// })

	embeds := discordapi.CreateEmbeds(params, embeddableFields)

	if len(embeds) > 10 {
		embeds = embeds[:10]
	}

	irErr := discordapi.InteractionRespond(c.Session, ic, nil, embeds, []discordgo.MessageComponent{})

	if irErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", irErr.Err), zap.String("error_message", irErr.Message), zap.Int("status_code", irErr.Code))
		logger := logging.Logger(ctx)
		logger.Error("error_log")

		return &Error{
			Message: "Failed to send error output",
			Err:     irErr,
		}
	}

	return nil
}

// SuccessOutput func
func (c *Commands) SuccessOutput(ctx context.Context, ic *discordgo.InteractionCreate, params discordapi.EmbeddableParams, embeddableFields []discordapi.EmbeddableField, embeddableErrors []discordapi.EmbeddableField) *Error {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if len(embeddableErrors) > 0 {
		params.Color = c.Config.Bot.WarnColor
	} else {
		params.Color = c.Config.Bot.OkColor
	}

	combinedFields := append(embeddableFields, embeddableErrors...)
	embeds := discordapi.CreateEmbeds(params, combinedFields)

	irErr := discordapi.InteractionRespond(c.Session, ic, nil, embeds[:1], []discordgo.MessageComponent{})

	if len(embeds) > 1 {
		for i := 1; i < len(embeds); i++ {
			_, smErr := discordapi.SendMessage(c.Session, ic.ChannelID, nil, embeds[i])
			if smErr != nil {
				smctx := logging.AddValues(ctx, zap.NamedError("error", smErr.Err), zap.String("error_message", smErr.Message), zap.Int("status_code", smErr.Code))
				logger := logging.Logger(smctx)
				logger.Error("error_log")
			}
		}
	}

	if irErr != nil {
		irctx := logging.AddValues(ctx, zap.NamedError("error", irErr.Err), zap.String("error_message", irErr.Message), zap.Int("status_code", irErr.Code))
		logger := logging.Logger(irctx)
		logger.Error("error_log")

		return &Error{
			Message: "Failed to send error output",
			Err:     irErr,
		}
	}

	return nil
}

// ConvertToEmbedField for Error struct
func (e *Error) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	name := e.Message
	value := e.Error()

	if name == "" {
		name = "Unknown Error Message"
	}

	if value == "" {
		value = "Unknown Error"
	}

	return &discordgo.MessageEmbedField{
		Name:   e.Message,
		Value:  e.Error(),
		Inline: false,
	}, nil
}

func parseSlashCommand(ic *discordgo.InteractionCreate) map[string]interface{} {
	var options map[string]interface{} = make(map[string]interface{})
	for _, option := range ic.ApplicationCommandData().Options {
		options[option.Name] = option.Value
	}

	return options
}

// HasApprovedRole func
func (c *Commands) HasApprovedRole(ctx context.Context, guild *gcscmodels.Guild, commandName string, roles []string) (bool, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if guild == nil {
		return false, nil
	}

	if guild.GuildServices == nil {
		return false, nil
	}

	for _, memberRole := range roles {
		for _, guildService := range guild.GuildServices {
			if guildService.Name != c.Config.Bot.GuildService {
				continue
			}

			if guildService.GuildServicePermissions == nil {
				return false, nil
			}

			for _, permission := range guildService.GuildServicePermissions {
				if permission.CommandName != commandName {
					continue
				}

				if permission.RoleID != memberRole {
					continue
				}

				return true, nil
			}
		}
	}

	return false, nil
}

// IsAdmin func
func (c *Commands) IsAdmin(ctx context.Context, guildID string, roles []string) (bool, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	discRoles, grErr := discordapi.GetGuildRoles(c.Session, guildID)
	if grErr != nil {
		return false, &Error{
			Message: "Failed to get Guild roles to verify Administrator access",
			Err:     grErr.Err,
		}
	}

	for _, memberRole := range roles {
		for _, guildRole := range discRoles {
			if memberRole == guildRole.ID && (guildRole.Permissions&discordgo.PermissionAdministrator == discordgo.PermissionAdministrator) {
				return true, nil
			}
		}
	}

	return false, nil
}

// IsOwner func
func (c *Commands) IsOwner(ctx context.Context, guild *discordgo.Guild, userID string) (bool, *Error) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if guild.OwnerID == userID {
		return true, nil
	}

	return false, nil
}

// IsApproved func
func (c *Commands) IsApproved(ctx context.Context, ic *discordgo.InteractionCreate, guildFeed *guild_feeds.GetGuildFeedByIDOK, command configs.Command) (bool, *Error) {
	if c.Session.State.Guilds == nil {
		return false, &Error{
			Message: "No state guild to check access",
			Err:     errors.New("nil state guild"),
		}
	}

	for _, aGuild := range c.Session.State.Guilds {
		if aGuild.ID == ic.GuildID {
			owner, ownerErr := c.IsOwner(ctx, aGuild, ic.Member.User.ID)
			if ownerErr != nil {
				return false, ownerErr
			}

			if owner {
				return owner, nil
			}

			break
		}
	}

	admin, adminErr := c.IsAdmin(ctx, ic.GuildID, ic.Member.Roles)
	if adminErr != nil {
		return false, adminErr
	}

	if admin {
		return true, nil
	}

	if guildFeed == nil || guildFeed.Payload == nil || guildFeed.Payload.Guild == nil {
		return false, nil
	}

	roleAccess, raErr := c.HasApprovedRole(ctx, guildFeed.Payload.Guild, command.Name, ic.Member.Roles)
	if raErr != nil {
		return false, raErr
	}

	if roleAccess {
		return true, nil
	}

	return false, nil
}
