package commands

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"go.uber.org/zap"
)

// GuildInfoSuccessOutput
type GuildInfoSuccessOutput struct {
	Guild GuildInfoGuild
}

// GuildInfoServerSuccessOutput
type GuildInfoServerSuccessOutput struct {
	Server GuildInfoServer
}

type GuildInfoGuild struct {
	Name                string
	ID                  string
	NitradoAccountCount int
	ServerCount         int
	BotsActivated       []string
	UserSetups          []string
}

type GuildInfoServer struct {
	Name           string
	ID             int64
	ChannelsLinked []string
}

// GuildInfoErrorOutput
type GuildInfoErrorOutput struct {
	Role    string
	Command string
}

func (c *Commands) GuildInfo(ctx context.Context, s *discordgo.Session, guildFeed *guild_feeds.GetGuildFeedByIDOK, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if _, ok := options["guild_id"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILD ID",
			Err:     errors.New("no guild id provided"),
		})
		return
	}

	lookupGuildFeed, lgfErr := guildconfigservice.GetGuildFeed(ctx, c.GuildConfigService, options["guild_id"].(string))
	if lgfErr != nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: lgfErr.Message,
			Err:     lgfErr,
		})
		return
	}

	if vErr := guildconfigservice.ValidateGuildFeed(lookupGuildFeed, c.Config.Bot.GuildService, "Guild"); vErr != nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: vErr.Message,
			Err:     vErr,
		})
		return
	}

	var successOutput *GuildInfoSuccessOutput
	var serversSuccessOutput []*GuildInfoServerSuccessOutput
	var errorOutput *GuildInfoErrorOutput

	successOutput = &GuildInfoSuccessOutput{
		Guild: GuildInfoGuild{
			Name:                lookupGuildFeed.Payload.Guild.Name,
			ID:                  lookupGuildFeed.Payload.Guild.ID,
			NitradoAccountCount: len(lookupGuildFeed.Payload.Guild.NitradoTokens),
			ServerCount:         len(lookupGuildFeed.Payload.Guild.Servers),
		},
	}

	if len(lookupGuildFeed.Payload.Guild.GuildServices) > 0 {
		for _, service := range lookupGuildFeed.Payload.Guild.GuildServices {
			switch service.Name {
			case "woodlands-status-bot-v2", "woodlands-nitrado-server-manager-v3":
				c.ErrorOutput(ctx, command, ic, Error{
					Message: "CANNOT LOOK UP THIS GUILD",
					Err:     errors.New("restricted guild"),
				})
				return
			}

			if !service.Enabled {
				continue
			}

			successOutput.Guild.BotsActivated = append(successOutput.Guild.BotsActivated, service.Name)
		}
	}

	if len(lookupGuildFeed.Payload.Guild.Setups) > 0 {
		var setupUserMap map[string]bool = make(map[string]bool)
		for _, user := range lookupGuildFeed.Payload.Guild.Setups {
			if user.Contact == nil {
				continue
			}

			if _, ok := setupUserMap[user.Contact.Name]; ok {
				continue
			}

			setupUserMap[user.Contact.Name] = true
			successOutput.Guild.UserSetups = append(successOutput.Guild.UserSetups, user.Contact.Name)
		}
	}

	if len(lookupGuildFeed.Payload.Guild.Servers) > 0 {
		for _, server := range lookupGuildFeed.Payload.Guild.Servers {
			var aServer *GuildInfoServerSuccessOutput = &GuildInfoServerSuccessOutput{
				Server: GuildInfoServer{
					Name: server.Name,
					ID:   server.NitradoID,
				},
			}

			if len(server.ServerOutputChannels) > 0 {
				for _, outputChannel := range server.ServerOutputChannels {
					if !outputChannel.Enabled {
						continue
					}

					aServer.Server.ChannelsLinked = append(aServer.Server.ChannelsLinked, outputChannel.OutputChannelTypeID)
				}
			}

			serversSuccessOutput = append(serversSuccessOutput, aServer)
		}
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	if successOutput != nil {
		embeddableFields = append(embeddableFields, successOutput)
	}

	if len(serversSuccessOutput) != 0 {
		if len(serversSuccessOutput) > 1 {
			sort.Slice(serversSuccessOutput, func(i, j int) bool {
				return serversSuccessOutput[i].Server.Name < serversSuccessOutput[j].Server.Name
			})
		}

		for _, server := range serversSuccessOutput {
			copyServer := *server
			embeddableFields = append(embeddableFields, &copyServer)
		}
	}

	if errorOutput != nil {
		embeddableErrors = append(embeddableErrors, errorOutput)
	}

	embedParams := discordapi.EmbeddableParams{
		Title:       "Guild Info",
		Description: fmt.Sprintf("Retrieved guild info for: %s.", options["guild_id"].(string)),
		TitleURL:    c.Config.Bot.DocumentationURL,
		Footer:      fmt.Sprintf("Executed by %s", ic.Member.User.Username),
	}

	c.SuccessOutput(ctx, ic, embedParams, embeddableFields, embeddableErrors)

	return
}

// ConvertToEmbedField for GuildInfoSuccessOutput struct
func (so *GuildInfoSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	var activatedBots string
	if len(so.Guild.BotsActivated) == 0 {
		activatedBots = "None"
	} else {
		for _, activatedBot := range so.Guild.BotsActivated {
			if activatedBots == "" {
				activatedBots = fmt.Sprintf("`%s`", activatedBot)
			} else {
				activatedBots += fmt.Sprintf(", `%s`", activatedBot)
			}
		}
	}

	var setupUsers string
	if len(so.Guild.UserSetups) == 0 {
		setupUsers = "None"
	} else {
		for _, user := range so.Guild.UserSetups {
			if setupUsers == "" {
				setupUsers = fmt.Sprintf("`%s`", user)
			} else {
				setupUsers += fmt.Sprintf(", `%s`", user)
			}
		}
	}

	fieldVal := fmt.Sprintf("**Guild Name:** `%s`\n**Guild ID:** `%s`\n**Nitrado Accounts:** `%d`\n**Game Servers:** `%d`\n**Activated Bots:** %s\n**Setup Users:** %s\n\u200B", so.Guild.Name, so.Guild.ID, so.Guild.NitradoAccountCount, so.Guild.ServerCount, activatedBots, setupUsers)

	return &discordgo.MessageEmbedField{
		Name:   "__**Guild Info**__",
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for GuildInfoServersSuccessOutput struct
func (so *GuildInfoServerSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	var channelsLinked string
	if len(so.Server.ChannelsLinked) == 0 {
		channelsLinked = "None"
	} else {
		if len(so.Server.ChannelsLinked) > 1 {
			sort.Slice(so.Server.ChannelsLinked, func(i, j int) bool {
				return so.Server.ChannelsLinked[i] < so.Server.ChannelsLinked[j]
			})
		}

		for _, channel := range so.Server.ChannelsLinked {
			if channelsLinked == "" {
				channelsLinked = fmt.Sprintf("`%s`", channel)
			} else {
				channelsLinked += fmt.Sprintf(", `%s`", channel)
			}
		}
	}

	fieldVal := fmt.Sprintf("**ID:** `%d`\n**Linked Channels:** %s\n\u200B", so.Server.ID, channelsLinked)

	return &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("**%s**", so.Server.Name),
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for GuildInfoErrorOutput struct
func (eo *GuildInfoErrorOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if eo.Command != "" {
		fieldVal = "```\n" + eo.Command + "\n```"
	} else {
		fieldVal = "```\nUnknown\n```"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Failed Access",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
