package commands

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
)

func (c *Commands) Help(ctx context.Context, s *discordgo.Session, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	fmt.Println("EXECUTED HELP COMMAND")
	fmt.Printf("\n%+v\n\n", options)
	c.ErrorOutput(ctx, command, ic, Error{
		Message: "HELP COMMAND",
		Err:     errors.New("help command executed"),
	})
	return
}
