package commands

import (
	"context"
	"errors"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"go.uber.org/zap"
)

// RemoveRoleSuccessOutput
type RemoveRoleSuccessOutput struct {
	Role    string
	Command string
}

// RemoveRoleErrorOutput
type RemoveRoleErrorOutput struct {
	Role    string
	Command string
}

func (c *Commands) RemoveRole(ctx context.Context, s *discordgo.Session, guildFeed *guild_feeds.GetGuildFeedByIDOK, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if _, ok := options["command"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO COMMAND",
			Err:     errors.New("no command provided"),
		})
		return
	}

	if _, ok := options["role"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO ROLE",
			Err:     errors.New("no role provided"),
		})
		return
	}

	commandName := options["command"].(string)
	roleID := options["role"].(string)

	var foundCommand *configs.Command
	for _, aCommand := range c.Config.Commands {
		if !aCommand.Enabled {
			continue
		}

		if aCommand.Name == commandName {
			foundCommand = &aCommand
			break
		}
	}

	if foundCommand == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "INVALID COMMAND",
			Err:     errors.New("command does not exist"),
		})
		return
	}

	var guildService *gcscmodels.GuildService
	for _, aGuildService := range guildFeed.Payload.Guild.GuildServices {
		if aGuildService.Name == c.Config.Bot.GuildService {
			guildService = aGuildService
			break
		}
	}

	if guildService == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILD SERVICE",
			Err:     errors.New("missing guild service"),
		})
		return
	}

	var guildServicePermission *gcscmodels.GuildServicePermission
	if guildService.GuildServicePermissions == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "ROLE DOES NOT HAVE ACCESS",
			Err:     errors.New("role does not have access to command"),
		})
		return
	} else {
		for _, permission := range guildService.GuildServicePermissions {
			if permission.RoleID == roleID && permission.CommandName == foundCommand.Name {
				guildServicePermission = permission
				break
			}
		}
	}

	if guildServicePermission == nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "ROLE DOES NOT HAVE ACCESS",
			Err:     errors.New("role does not have access to command"),
		})
		return
	}

	var successOutput *RemoveRoleSuccessOutput
	var errorOutput *RemoveRoleErrorOutput

	_, gspErr := guildconfigservice.DeleteGuildServicePermission(ctx, c.GuildConfigService, ic.GuildID, int64(guildServicePermission.GuildServiceID))
	if gspErr != nil {
		newCtx := logging.AddValues(ctx,
			zap.NamedError("error", gspErr),
			zap.String("error_message", gspErr.Message),
			zap.String("role_id", roleID),
			zap.String("permission_command", commandName),
		)
		logger := logging.Logger(newCtx)
		logger.Error("error_log")

		errorOutput = &RemoveRoleErrorOutput{
			Command: commandName,
			Role:    roleID,
		}
	} else {
		successOutput = &RemoveRoleSuccessOutput{
			Command: commandName,
			Role:    roleID,
		}
	}

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	if successOutput != nil {
		embeddableFields = append(embeddableFields, successOutput)
	}

	if errorOutput != nil {
		embeddableErrors = append(embeddableErrors, errorOutput)
	}

	embedParams := discordapi.EmbeddableParams{
		Title:       "Removed Role Access",
		Description: fmt.Sprintf("Removed role access for: <@&%s>.", guildServicePermission.RoleID),
		TitleURL:    c.Config.Bot.DocumentationURL,
		Footer:      fmt.Sprintf("Executed by %s", ic.Member.User.Username),
	}

	c.SuccessOutput(ctx, ic, embedParams, embeddableFields, embeddableErrors)

	return
}

// ConvertToEmbedField for RemoveRoleSuccessOutput struct
func (so *RemoveRoleSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if so.Command != "" {
		fieldVal = "```\n" + so.Command + "\n```"
	} else {
		fieldVal = "```\nUnknown\n```"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Removed Access",
		Value:  fieldVal,
		Inline: false,
	}, nil
}

// ConvertToEmbedField for RemoveRoleErrorOutput struct
func (eo *RemoveRoleErrorOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := ""

	if eo.Command != "" {
		fieldVal = "```\n" + eo.Command + "\n```"
	} else {
		fieldVal = "```\nUnknown\n```"
	}

	return &discordgo.MessageEmbedField{
		Name:   "Failed Removing Access",
		Value:  fieldVal,
		Inline: false,
	}, nil
}
