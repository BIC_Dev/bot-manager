package commands

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"github.com/bwmarrin/discordgo"
	"github.com/go-openapi/strfmt"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/discordapi"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_feeds"
	"gitlab.com/BIC_Dev/guild-config-service-client/gcscmodels"
	"go.uber.org/zap"
)

// UserGuildSuccessOutput
type UserGuildSuccessOutput struct {
	GuildID    string
	GuildName  string
	CreateDate strfmt.DateTime
}

func (c *Commands) UserGuild(ctx context.Context, s *discordgo.Session, guildFeed *guild_feeds.GetGuildFeedByIDOK, ic *discordgo.InteractionCreate, options map[string]interface{}, command configs.Command) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	if _, ok := options["user_id"]; !ok {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO USER ID",
			Err:     errors.New("no user id provided"),
		})
		return
	}

	userID := options["user_id"].(string)

	allGuildServiceContacts, agscErr := guildconfigservice.GetAllGuildServiceContacts(ctx, c.GuildConfigService)
	if agscErr != nil {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: agscErr.Message,
			Err:     agscErr,
		})
		return
	}

	if len(allGuildServiceContacts.Payload.GuildServiceContacts) == 0 {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILD SERVICES CONTACTS FOUND",
			Err:     errors.New("no guild service contacts"),
		})
		return
	}

	var guildServiceIDs []uint64
	for _, guildServiceContact := range allGuildServiceContacts.Payload.GuildServiceContacts {
		if guildServiceContact.ContactID == userID {
			guildServiceIDs = append(guildServiceIDs, guildServiceContact.GuildServiceID)
		}
	}

	if len(guildServiceIDs) == 0 {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILD SERVICES FOUND FOR USER",
			Err:     errors.New("user does not have guild service"),
		})
		return
	}

	var guildIDs map[string]bool = make(map[string]bool)
	for _, guildServiceID := range guildServiceIDs {
		guildService, gsErr := guildconfigservice.GetGuildServiceByID(ctx, c.GuildConfigService, ic.GuildID, int64(guildServiceID))
		if gsErr != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: gsErr.Message,
				Err:     gsErr,
			})
			return
		}

		guildIDs[guildService.Payload.GuildService.GuildID] = true
	}

	if len(guildIDs) == 0 {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILDS FOUND",
			Err:     errors.New("user does not have a guild"),
		})
		return
	}

	var guilds []*gcscmodels.Guild
	for guildID := range guildIDs {
		guild, gErr := guildconfigservice.GetGuildByID(ctx, c.GuildConfigService, guildID)
		if gErr != nil {
			c.ErrorOutput(ctx, command, ic, Error{
				Message: gErr.Message,
				Err:     gErr,
			})
			return
		}

		guilds = append(guilds, guild.Payload.Guild)
	}

	if len(guildIDs) == 0 {
		c.ErrorOutput(ctx, command, ic, Error{
			Message: "NO GUILDS FOUND",
			Err:     errors.New("guild services do not have assigned guilds"),
		})
		return
	}

	var userGuildSuccessOutputs []*UserGuildSuccessOutput
	for _, guild := range guilds {
		userGuildSuccessOutputs = append(userGuildSuccessOutputs, &UserGuildSuccessOutput{
			GuildID:    guild.ID,
			GuildName:  guild.Name,
			CreateDate: guild.CreatedAt,
		})
	}

	sort.Slice(userGuildSuccessOutputs, func(i, j int) bool {
		return userGuildSuccessOutputs[i].GuildName < userGuildSuccessOutputs[j].GuildName
	})

	var embeddableFields []discordapi.EmbeddableField
	var embeddableErrors []discordapi.EmbeddableField

	embedParams := discordapi.EmbeddableParams{
		Title:       "User Guilds",
		Description: fmt.Sprintf("Got all guilds associated with: <@%s>.", userID),
		TitleURL:    c.Config.Bot.DocumentationURL,
		Footer:      fmt.Sprintf("Executed by %s", ic.Member.User.Username),
	}

	for _, outputs := range userGuildSuccessOutputs {
		output := *outputs
		embeddableFields = append(embeddableFields, &output)
	}

	c.SuccessOutput(ctx, ic, embedParams, embeddableFields, embeddableErrors)
}

// ConvertToEmbedField for AddRoleSuccessOutput struct
func (so *UserGuildSuccessOutput) ConvertToEmbedField() (*discordgo.MessageEmbedField, *discordapi.Error) {
	fieldVal := fmt.Sprintf("**Guild ID:** `%s`\n**Create Date:** `%s`\n\u200B", so.GuildID, so.CreateDate.String())
	name := fmt.Sprintf("**%s**", so.GuildName)

	if name == "" || name == "****" {
		name = "**Unknown**"
	}

	return &discordgo.MessageEmbedField{
		Name:   name,
		Value:  fieldVal,
		Inline: false,
	}, nil
}
