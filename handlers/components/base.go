package components

import (
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/services/nsm"
)

// Components struct
type Components struct {
	Session                  *discordgo.Session
	Config                   *configs.Config
	GuildConfigService       *guildconfigservice.GuildConfigService
	NitradoServerManager     *nsm.NSM
	MessagesAwaitingReaction *MessagesAwaitingReaction
}

type MessagesAwaitingReaction struct {
	Messages map[string]MessageAwaitingReaction
}

// MessageAwaitingReaction struct
type MessageAwaitingReaction struct {
	Expires     int64
	Reactions   []string
	CommandName string
	User        string
}

// Error struct
type Error struct {
	Message string `json:"message"`
	Err     error  `json:"error"`
}

// Error func
func (e *Error) Error() string {
	return e.Err.Error()
}

// ExpireMessagesAwaitingReaction func
func ExpireMessagesAwaitingReaction(messagesAwaitingReaction *MessagesAwaitingReaction) {
	ticker := time.NewTicker(60 * time.Second)

	for range ticker.C {
		for key, messageAwaitingReaction := range messagesAwaitingReaction.Messages {
			if messageAwaitingReaction.Expires < time.Now().Unix() {
				delete(messagesAwaitingReaction.Messages, key)
			}
		}
	}
}
