package main

import (
	"context"
	"log"

	"github.com/bwmarrin/discordgo"
	"github.com/caarlos0/env"
	"gitlab.com/BIC_Dev/bot-manager/configs"
	"gitlab.com/BIC_Dev/bot-manager/controllers"
	"gitlab.com/BIC_Dev/bot-manager/handlers"
	"gitlab.com/BIC_Dev/bot-manager/routes"
	"gitlab.com/BIC_Dev/bot-manager/services/guildconfigservice"
	"gitlab.com/BIC_Dev/bot-manager/services/nsm"
	"gitlab.com/BIC_Dev/bot-manager/utils/logging"
	"go.uber.org/zap"
)

// Environment struct
type Environment struct {
	Environment                      string `env:"ENVIRONMENT,required"`
	DiscordToken                     string `env:"DISCORD_TOKEN,required"`
	ListenerPort                     string `env:"LISTENER_PORT,required"`
	ServiceToken                     string `env:"SERVICE_TOKEN,required"`
	GuildConfigServiceToken          string `env:"GUILD_CONFIG_SERVICE_TOKEN,required"`
	NitradoServerManagerServiceToken string `env:"NITRADO_SERVER_MANAGER_SERVICE_TOKEN,required"`
	BasePath                         string `env:"BASE_PATH"`
}

func main() {
	ctx := context.Background()
	environment := Environment{}
	if err := env.Parse(&environment); err != nil {
		log.Fatal("FAILED TO LOAD CONFIG")
	}

	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("env", environment.Environment),
		zap.String("listener_port", environment.ListenerPort),
		zap.String("base_path", environment.BasePath),
	)

	config := configs.GetConfig(ctx, environment.Environment)
	guildConfigService := guildconfigservice.InitService(ctx, config, environment.GuildConfigServiceToken)

	// Instantiate Discord client
	dg, discErr := discordgo.New("Bot " + environment.DiscordToken)
	if discErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", discErr), zap.String("error_message", "Failed to create Discord client"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	defer dg.Close()

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", openErr), zap.String("error_message", "Failed to open Discord web socket"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	comm := handlers.Handlers{
		Session:            dg,
		Config:             config,
		GuildConfigService: guildConfigService,
		NitradoServerManager: &nsm.NSM{
			BasePath:     config.NitradoServerManager.Host + config.NitradoServerManager.BasePath,
			ServiceToken: environment.NitradoServerManagerServiceToken,
		},
	}

	comm.AddSlashCommands(ctx, environment.Environment)

	comm.SetupHandlers()

	router := routes.GetRouter(ctx)
	controller := controllers.Controller{
		Config:             config,
		DiscordSession:     dg,
		GuildConfigService: guildConfigService,
	}

	r := routes.Router{
		ServiceToken: environment.ServiceToken,
		Port:         environment.ListenerPort,
		BasePath:     environment.BasePath,
		Controller:   &controller,
	}

	routes.AddRoutes(ctx, router, r)
}
