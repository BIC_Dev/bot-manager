package discordapi

import "github.com/bwmarrin/discordgo"

// InteractionRespond func
func InteractionRespond(session *discordgo.Session, ic *discordgo.InteractionCreate, content *string, embeds []*discordgo.MessageEmbed, components []discordgo.MessageComponent) *Error {
	var interactionResponseData *discordgo.InteractionResponseData = &discordgo.InteractionResponseData{}

	if embeds != nil {
		interactionResponseData.Embeds = embeds
	}

	if content != nil {
		interactionResponseData.Content = *content
	}

	if len(components) > 0 {
		interactionResponseData.Components = components
	}

	irErr := session.InteractionRespond(ic.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseChannelMessageWithSource,
		Data: interactionResponseData,
	})

	if irErr != nil {
		return ParseDiscordError(irErr)
	}

	return nil
}
