package guildconfigservice

import (
	"context"
	"fmt"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_service_contacts"
)

// GetAllGuildServiceContacts func
func GetAllGuildServiceContacts(ctx context.Context, gcs *GuildConfigService) (*guild_service_contacts.GetAllGuildServiceContactsOK, *Error) {
	guildServiceContactsParams := guild_service_contacts.NewGetAllGuildServiceContactsParamsWithTimeout(30)
	guildServiceContactsParams.Context = context.Background()

	guildServiceContacts, gscErr := gcs.Client.GuildServiceContacts.GetAllGuildServiceContacts(guildServiceContactsParams, gcs.Auth)
	if gscErr != nil {
		if val, ok := gscErr.(*guild_service_contacts.GetAllGuildServiceContactsNotFound); ok {
			return guildServiceContacts, &Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			}
		}

		return guildServiceContacts, &Error{
			Message: "Failed to get guild service contacts",
			Err:     gscErr,
		}
	}

	return guildServiceContacts, nil
}
