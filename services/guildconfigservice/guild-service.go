package guildconfigservice

import (
	"context"
	"fmt"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guild_services"
)

// GetAllGuildServices func
func GetAllGuildServices(ctx context.Context, gcs *GuildConfigService) (*guild_services.GetAllGuildServicesOK, *Error) {
	guildServicesParams := guild_services.NewGetAllGuildServicesParamsWithTimeout(30)
	guildServicesParams.Context = context.Background()

	guildServices, gscErr := gcs.Client.GuildServices.GetAllGuildServices(guildServicesParams, gcs.Auth)
	if gscErr != nil {
		if val, ok := gscErr.(*guild_services.GetAllGuildServicesNotFound); ok {
			return guildServices, &Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			}
		}

		return guildServices, &Error{
			Message: "Failed to get guild services",
			Err:     gscErr,
		}
	}

	return guildServices, nil
}

// GetGuildServiceByID func
func GetGuildServiceByID(ctx context.Context, gcs *GuildConfigService, guildID string, guildServiceID int64) (*guild_services.GetGuildServiceByIDOK, *Error) {
	params := guild_services.NewGetGuildServiceByIDParamsWithTimeout(30)
	params.GuildServiceID = guildServiceID
	params.Guild = guildID
	params.Context = context.Background()

	guildService, gscErr := gcs.Client.GuildServices.GetGuildServiceByID(params, gcs.Auth)
	if gscErr != nil {
		if val, ok := gscErr.(*guild_services.GetAllGuildServicesNotFound); ok {
			return guildService, &Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			}
		}

		return guildService, &Error{
			Message: "Failed to get guild service by id",
			Err:     gscErr,
		}
	}

	return guildService, nil
}
