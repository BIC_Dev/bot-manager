package guildconfigservice

import (
	"context"
	"fmt"

	"gitlab.com/BIC_Dev/guild-config-service-client/gcsc/guilds"
)

// GetGuildByID func
func GetGuildByID(ctx context.Context, gcs *GuildConfigService, guildID string) (*guilds.GetGuildByIDOK, *Error) {
	params := guilds.NewGetGuildByIDParamsWithTimeout(30)
	params.GuildID = guildID
	params.Guild = guildID
	params.Context = context.Background()

	guild, gscErr := gcs.Client.Guilds.GetGuildByID(params, gcs.Auth)
	if gscErr != nil {
		if val, ok := gscErr.(*guilds.GetAllGuildsNotFound); ok {
			return guild, &Error{
				Message: "Discord server not set up",
				Err:     fmt.Errorf("%s\n%s", val.Payload.Message, val.Payload.Error),
			}
		}

		return guild, &Error{
			Message: "Failed to get guild by id",
			Err:     gscErr,
		}
	}

	return guild, nil
}
