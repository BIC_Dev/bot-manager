package nsm

// CreateActivationTokenRequest struct
type CreateActivationTokenRequest struct {
}

// CreateActivationTokenResponse struct
type CreateActivationTokenResponse struct {
	Message    string          `json:"message"`
	SetupToken ActivationToken `json:"setup_token"`
}

// ActivationToken struct
type ActivationToken struct {
	Token string `json:"token"`
}

func (nsm *NSM) CreateActivationToken() (*CreateActivationTokenResponse, *Error) {
	var activationTokenResponse CreateActivationTokenResponse

	err := Request(&activationTokenResponse, "POST", nsm.BasePath+"/activation-tokens", nil, CreateActivationTokenRequest{}, map[string]string{
		"Service-Token": nsm.ServiceToken,
	})

	if err != nil {
		return nil, err
	}

	return &activationTokenResponse, nil
}
