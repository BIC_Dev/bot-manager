package nsm

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

// NSMV3 struct
type NSM struct {
	BasePath     string `json:"base_path"`
	ServiceToken string `json:"service_token"`
}

// ErrorResponse response struct for an error
type ErrorResponse struct {
	Status  int    `json:"-"`
	Error   string `json:"error"`
	Message string `json:"message"`
}

// Error struct
type Error struct {
	Status  int    `json:"-"`
	Err     error  `json:"err"`
	Message string `json:"message"`
}

func (e *Error) Error() string {
	return e.Err.Error()
}

// Request handler
func Request(output interface{}, method string, url string, params map[string]string, body interface{}, headers map[string]string) *Error {
	httpClient := &http.Client{
		Timeout: time.Second * 30,
	}

	var requestBody *bytes.Buffer
	var newRequest *http.Request
	var newRequestErr error

	if body != nil {
		jsonBody, jsonErr := json.Marshal(body)

		if jsonErr != nil {
			return &Error{
				Status:  http.StatusInternalServerError,
				Message: "Could not marshal request struct",
				Err:     jsonErr,
			}
		}

		requestBody = bytes.NewBuffer(jsonBody)

		newRequest, newRequestErr = http.NewRequest(method, url, requestBody)
	} else {
		newRequest, newRequestErr = http.NewRequest(method, url, nil)
	}

	if newRequestErr != nil {
		return &Error{
			Status:  http.StatusInternalServerError,
			Message: "Failed to create request",
			Err:     newRequestErr,
		}
	}

	if params != nil {
		q := newRequest.URL.Query()

		for key, val := range params {
			q.Add(key, val)
		}

		newRequest.URL.RawQuery = q.Encode()
	}

	for k, v := range headers {
		newRequest.Header.Add(k, v)
	}

	response, requestErr := httpClient.Do(newRequest)

	if requestErr != nil {
		return &Error{
			Status:  http.StatusFailedDependency,
			Message: "Request failed",
			Err:     requestErr,
		}
	}

	switch response.StatusCode {
	case http.StatusOK, http.StatusCreated:
		jsonErr := json.NewDecoder(response.Body).Decode(&output)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			return &Error{
				Status:  http.StatusInternalServerError,
				Message: "Bad response from api endpoint",
				Err:     fmt.Errorf(bodyString),
			}
		}

		return nil
	default:
		errorResponse := ErrorResponse{}
		jsonErr := json.NewDecoder(response.Body).Decode(&errorResponse)

		if jsonErr != nil {
			bodyBytes, _ := ioutil.ReadAll(response.Body)
			bodyString := string(bodyBytes)

			return &Error{
				Status:  response.StatusCode,
				Message: "Bad response from api endpoint",
				Err:     fmt.Errorf(bodyString),
			}
		}

		return &Error{
			Status:  response.StatusCode,
			Message: errorResponse.Message,
			Err:     fmt.Errorf(errorResponse.Error),
		}
	}
}
