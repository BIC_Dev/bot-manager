package viewmodels

// ActivateBotRequest struct
type ActivateBotRequest struct {
	GuildID string `json:"guild_id"`
}

// ActivateBotResponse struct
type ActivateBotResponse struct {
	Message string `json:"message"`
}
